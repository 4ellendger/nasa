﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Rust.Models
{
    /// <summary>
    /// Contains robot status information.
    /// </summary>
    [DataContract]
    public class RobotStatus
    {
        public RobotStatus()
        {
            this.Time = DateTime.UtcNow;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets robot exploration strategy name.
        /// </summary>
        [DataMember(Name="strategy")]
        public string StrategyName { get; set; }

        [DataMember(Name = "mode")]
        public string Mode { get; set; }

        [DataMember(Name = "handledTargets")]
        public List<string> HandledTargets { get; set; }

        [DataMember(Name = "angle")]
        public double Angle { get; set; }

        [DataMember(Name = "targetAngle")]
        public double TargetAngle { get; set; }

        [DataMember(Name = "obstacleAngle")]
        public double obstacleAngle { get; set; }

        [DataMember(Name = "time")]
        public DateTime Time { get; set; }
    }
}