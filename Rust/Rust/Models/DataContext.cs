﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Rust.Models
{
    public class DataContext:DbContext
    {
        public DataContext() : base("name=DefaultConnection")
        {
        }

        public DbSet<RobotStatus> Statuses { get; set; }
    }
}