﻿using System.Linq;
using System.Web.Http.Description;
using Rust.Models;

namespace Rust.Controllers
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    /// <summary>
    /// Contains methods to receive information from robot.
    /// </summary>
    [RoutePrefix("api/robot")]
    public class RobotController : ApiController
    {
        /// <summary>
        /// Method to send robot status to server.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="status">Robot status object.</param>
        /// <returns>Response with 200 OK status if everything is good.</returns>
        [HttpGet]
        [Route("LastFrame")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage GetLastFrameName(HttpRequestMessage request)
        {
            return request.CreateResponse(HttpStatusCode.OK, Core.Configuration.LastFramePath);
        }

        [HttpGet]
        [Route("LastFrame2")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage GetLastFrameName2(HttpRequestMessage request)
        {
            return request.CreateResponse(HttpStatusCode.OK, Core.Configuration.LastFramePath2);
        }

        [HttpGet]
        [Route("Logs")]
        [ResponseType(typeof(RobotStatus))]
        public HttpResponseMessage GetLogs(HttpRequestMessage request)
        {
            DataContext db = new DataContext();
            return request.CreateResponse(HttpStatusCode.OK, db.Statuses.Select(r => r));
        }
    }
}