﻿$(function () {

    var currentViewId = "#logsPanel";

    var inTransition = false;

    var shouldVideoUpdate = false;
    var shouldVideoUpdate2 = false;
    var shouldLogsUpdate = true;

    var l = {};
    l.logs = ko.observableArray();
    
    ko.applyBindings(l, $("#logsPanel").get(0));

    var translateScreen = function(newId) {
        if (currentViewId == newId || inTransition) return;
        inTransition = true;
        $(currentViewId).hide("slide", function () {
            $(newId).show("slide", function () {
                currentViewId = newId;
                inTransition = false;
            });
        });
    };

    $("#showLogs").click(function () {
        translateScreen("#logsPanel");
        shouldVideoUpdate = false;
        shouldLogsUpdate = true;
        shouldVideoUpdate2 = false;
    });

    $("#showVideoStream").click(function () {
        translateScreen("#videoStreamPanel");
        shouldVideoUpdate = true;
        shouldLogsUpdate = false;
        shouldVideoUpdate2 = false;
    });
    
    $("#showTargets").click(function () {
        translateScreen("#targetsPanel");
        shouldVideoUpdate = false;
        shouldLogsUpdate = false; shouldVideoUpdate2 = true;
    });
    
    function updateVideo() {
        if (!shouldVideoUpdate) return;
        return ajaxRequest("get", "/api/robot/LastFrame")
            .done(function (result) {
                $("#videoStream").attr('src', 'content/frames/' + result);
            });
    }
    
    function updateVideo2() {
        if (!shouldVideoUpdate2) return;
        return ajaxRequest("get", "/api/robot/LastFrame2")
            .done(function (result) {
                $("#videoStream2").attr('src', 'content/frames2/' + result);
            });
    }
    
    function updateLogs() {
        if (!shouldLogsUpdate) return;
        return ajaxRequest("get", "/api/robot/Logs")
            .done(function (result) {
                l.logs(result);
            });
    }
    
    function ajaxRequest(type, url, data, dataType) { // Ajax helper
        var options = {
            dataType: dataType || "json",
            contentType: "application/json",
            cache: false,
            type: type,
            data: data ? data.toJson() : null
        };
        
        return $.ajax(url, options);
    }

    setInterval(updateVideo, 1000);
    setInterval(updateVideo2, 1000);
    setInterval(updateLogs, 1000);
});