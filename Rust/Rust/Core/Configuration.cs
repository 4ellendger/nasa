﻿using System.Configuration;
using System.IO;
using System.Net.Mime;

namespace Rust.Core
{
    /// <summary>
    /// Static class with configurations.
    /// </summary>
    internal static class Configuration
    {
        /// <summary>
        /// Gets the robot host.
        /// </summary>
        public static string RobotHost
        {
            get { return ConfigurationManager.AppSettings["RobotHost"]; }
        }

        private static int _lastFrameNumber = 0;

        private static bool _lastFrameAccessable = false;

        private static int _lastFrameNumber2 = 0;

        private static bool _lastFrameAccessable2 = false;

        private static readonly string FramePath = @"Frame";

        public static string LastFramePath2
        {
            get
            {

                return FramePath + _lastFrameNumber2.ToString("0000") + ".jpg";
            }
        }

        public static string LastFramePath
        {
            get
            {

                return FramePath + _lastFrameNumber.ToString("0000") + ".jpg";
            }
        }

        public static string GetNextFrameName()
        {
            if (_lastFrameAccessable)
            {
                _lastFrameNumber++;
                //_lastFrameNumber = _lastFrameNumber%25;
            }
            _lastFrameAccessable = false;
            return LastFramePath;
        }

        public static void CheckFrameAsRetrived()
        {
            _lastFrameAccessable = true;
        }

        public static string GetNextFrameName2()
        {
            if (_lastFrameAccessable2)
            {
                _lastFrameNumber2++;
                //_lastFrameNumber = _lastFrameNumber%25;
            }
            _lastFrameAccessable2 = false;
            return LastFramePath2;
        }

        public static void CheckFrameAsRetrived2()
        {
            _lastFrameAccessable2 = true;
        }
    }
}