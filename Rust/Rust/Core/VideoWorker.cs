﻿using System.Threading;

namespace Rust.Core
{
    public class VideoWorker: IWorker
    {
        protected volatile bool _shouldStop;

        public virtual void DoWork(object anObject)
        {
            while (!_shouldStop)
            {
                RobotManager.GetImageFromRobot();
                RobotManager.GetImageFromRobot2();

                // Do some work
                Thread.Sleep(650);
            }

            // thread is stopping
            // Do some final work
        }

        public void Stop()
        {
            _shouldStop = true;
        }

        // Volatile is used as hint to the compiler that this data
        // member will be accessed by multiple threads.
    }
}