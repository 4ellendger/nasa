﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Rust.Models;

namespace Rust.Core
{
    /// <summary>
    /// Contains methods to send or retrieve information from the robot.
    /// </summary>
    public static class RobotManager
    {
        public static void GetRobotStatus()
        {
            string uri = Configuration.RobotHost + "/status";

            WebRequest request = WebRequest.Create(uri);
            request.Method = "GET";

            StreamReader reader = null;
            HttpWebResponse response = null;
            Stream dataStream = null;

            try
            {
                // Get the response.
                response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return;
                }

                // Get the stream containing content returned by the server.
                dataStream = response.GetResponseStream();

                if (dataStream == null)
                {
                    return;
                }

                // Open the stream using a StreamReader for easy access.
                reader = new StreamReader(dataStream);

                // Read the content.
                string responseFromServer = reader.ReadToEnd();

                var status = JsonConvert.DeserializeObject<RobotStatus>(responseFromServer);

                DataContext db = new DataContext();

                db.Statuses.Add(status);
                db.SaveChanges();
            }
            catch (WebException e)
            {

            }
            finally
            {
                // Clean up the streams.
                if (reader != null) reader.Close();
                if (dataStream != null) dataStream.Close();
                if (response != null) response.Close();
            }
        }

        public static void GetImageFromRobot2()
        {
            string uri = Configuration.RobotHost + "/lastTarget";

            WebRequest request = WebRequest.Create(uri);
            request.Method = "GET";


            HttpWebResponse response = null;
            Stream dataStream = null;
            FileStream fs = null;
            try
            {
                fs =
                    new FileStream(
                        AppDomain.CurrentDomain.BaseDirectory + "\\Content\\Frames2\\" + Configuration.GetNextFrameName2(),
                        FileMode.Create);


                try
                {
                    // Get the response.
                    response = (HttpWebResponse)request.GetResponse();

                    if (response.StatusCode != HttpStatusCode.OK) return;

                    dataStream = response.GetResponseStream();

                    if (dataStream == null) return;
                    Task a = dataStream.CopyToAsync(fs);
                    a.Wait();
                }
                catch (WebException e)
                {
                    return;
                }
                finally
                {
                    // Clean up the streams.
                    if (dataStream != null) dataStream.Close();
                    if (response != null) response.Close();
                }
            }
            catch (IOException e)
            {

            }
            finally
            {
                fs.Close();
            }

            Configuration.CheckFrameAsRetrived2();
        }

        public static void GetImageFromRobot()
        {
            string uri = Configuration.RobotHost + "/frame";

            WebRequest request = WebRequest.Create(uri);
            request.Method = "GET";


            HttpWebResponse response = null;
            Stream dataStream = null;
            FileStream fs = null;
            try
            {
                fs =
                    new FileStream(
                        AppDomain.CurrentDomain.BaseDirectory + "\\Content\\Frames\\" + Configuration.GetNextFrameName(),
                        FileMode.Create);


                try
                {
                    // Get the response.
                    response = (HttpWebResponse) request.GetResponse();

                    if (response.StatusCode != HttpStatusCode.OK) return;

                    dataStream = response.GetResponseStream();

                    if (dataStream == null) return;
                    Task a = dataStream.CopyToAsync(fs);
                    a.Wait();
                }
                catch (WebException e)
                {
                    return;
                }
                finally
                {
                    // Clean up the streams.
                    if (dataStream != null) dataStream.Close();
                    if (response != null) response.Close();
                }
            }
            catch (IOException e)
            {

            }
            finally
            {
                fs.Close();
            }

            Configuration.CheckFrameAsRetrived();
        }
    }
}