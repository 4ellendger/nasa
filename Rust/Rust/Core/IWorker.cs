﻿namespace Rust.Core
{
    public interface IWorker
    {
        void DoWork(object anObject);
        
        void Stop();
    }
}