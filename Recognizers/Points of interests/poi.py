import numpy as np
import cv2
#from matplotlib import pyplot as plt
import sys
sys.path.append(".")
from video import start


# Function to discover if Pink color present in the given frame, returns the coordinates of frame
def discover_color(frame, row, column):
    frame = cv2.medianBlur(frame,5)
    small = cv2.pyrDown(frame)
    BGR_Borders = [[150 / 255.0 , 1806 / 255.0], [60 / 255.0 , 80 / 255.0],[254 / 255.0 , 256 / 255.0]] #Pink color in BGR colors
    #for width_step in range(width/20):
    #small_step=small[0:heigth, width_step*20:width_step*20+width_step]
    hsv = cv2.cvtColor(small, cv2.COLOR_BGR2HSV)
    dark = hsv[...,2] < 32
    hsv[dark] = 0
    h = cv2.calcHist( [hsv], [0, 1], None, [180, 256], [0, 180, 0, 256] )
    h = np.clip(h*0.005*hist_scale, 0, 1)
    vis = hsv_map*h[:,:,np.newaxis]  / 255.0
    cv2.imshow('hist', vis) #show histogram

    for element in vis[160:180]: #Check range of dots in histogram to find out if Pink is present
        for element2 in element:
            if (element2[1]>BGR_Borders[1][0] and element2[1]<BGR_Borders[1][1]):
                if (element2[2]>BGR_Borders[2][0] and element2[2]<BGR_Borders[2][1]):
                    if (element2[0]>BGR_Borders[0][0] and element2[0]<BGR_Borders[0][1]):
                        #return "PINK!! | ", "Row:", row, " Col: ", column
                        return [row, column]

def cb(frameL, frameR):
    frame = np.concatenate((frameL, frameR), axis=1)
    cv2.imwrite("frame.jpg", frame)

    #frame = cv2.medianBlur(frame,5)
    #cv2.imshow('camera', frame) #show video
    frameL = cv2.pyrDown(frameL)
    heigth, width, depth = frameL.shape
    zones_count=3 # zones_count^2 - number of zones to separate frame
    heigth = heigth/zones_count
    width = width/zones_count

    #discover_color(frameL, 0, 0)
    #cv2.waitKey(0)

    for frame_row in range(zones_count):
        for frame_column in range(zones_count):
            #Return position of the color in the frame
            #return discover_color(frameL[heigth*frame_row:heigth*(frame_row+1), width*frame_column:width*(frame_column+1)], frame_row, frame_column)
            coord = discover_color(frameL[heigth*frame_row:heigth*(frame_row+1), width*frame_column:width*(frame_column+1)], frame_row, frame_column)
            return "9: %f, %f" % coord[0], coord[1]



hsv_map = np.zeros((180, 256, 3), np.uint8)
h, s = np.indices(hsv_map.shape[:2])
hsv_map[:,:,0] = h
hsv_map[:,:,1] = s
hsv_map[:,:,2] = 255
hsv_map = cv2.cvtColor(hsv_map, cv2.COLOR_HSV2BGR)
hist_scale = 64
start(cb, 100)
