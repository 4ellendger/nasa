#!/usr/bin/env python

import numpy as np
import cv2
import sys
import video

if __name__ == '__main__':

    hsv_map = np.zeros((180, 256, 3), np.uint8)
    h, s = np.indices(hsv_map.shape[:2])
    hsv_map[:,:,0] = h
    hsv_map[:,:,1] = s
    hsv_map[:,:,2] = 255
    hsv_map = cv2.cvtColor(hsv_map, cv2.COLOR_HSV2BGR)
    #cv2.imshow('hsv_map', hsv_map)

    #cv2.namedWindow('hist', 0)
    hist_scale = 64
    #def set_scale(val):
        #global hist_scale
        #hist_scale = val
    #cv2.createTrackbar('scale', 'hist', hist_scale, 32, set_scale)


# Function to discover if Pink color present in the given frame, returns the coordinates of frame
    def discover_color(frame, row, column):

        frame = cv2.medianBlur(frame,5)
        small = cv2.pyrDown(frame)
        BGR_Borders = [[150 / 255.0 , 1806 / 255.0], [60 / 255.0 , 80 / 255.0],[254 / 255.0 , 256 / 255.0]] #Pink color in BGR colors
        #for width_step in range(width/20):
        #small_step=small[0:heigth, width_step*20:width_step*20+width_step]
        hsv = cv2.cvtColor(small, cv2.COLOR_BGR2HSV)
        dark = hsv[...,2] < 32
        hsv[dark] = 0
        h = cv2.calcHist( [hsv], [0, 1], None, [180, 256], [0, 180, 0, 256] )
        h = np.clip(h*0.005*hist_scale, 0, 1)
        vis = hsv_map*h[:,:,np.newaxis]  / 255.0
        # cv2.imshow('hist', vis) #show histogram

        for element in vis[160:180]: #Check range of dots in histogram to find out if Pink is present
            for element2 in element:
                if (element2[1]>BGR_Borders[1][0] and element2[1]<BGR_Borders[1][1]):
                    if (element2[2]>BGR_Borders[2][0] and element2[2]<BGR_Borders[2][1]):
                        if (element2[0]>BGR_Borders[0][0] and element2[0]<BGR_Borders[0][1]):
                           #return "PINK!! | ", "Row:", row, " Col: ", column
                            return [row, column]


    try: fn = sys.argv[1]
    except: fn = 0
    cam = video.create_capture(fn, fallback='synth:bg=../cpp/baboon.jpg:class=chess:noise=0.05')


    while True:
        flag, frame = cam.read()
        #frame = cv2.medianBlur(frame,5)
        #cv2.imshow('camera', frame) #show video
        frame = cv2.pyrDown(frame)
        heigth, width, depth = frame.shape
        zones_count=3 # zones_count^2 - number of zones to separate frame
        heigth = heigth/zones_count
        width = width/zones_count
        for frame_row in range(zones_count):
            for frame_column in range(zones_count):
                #Return position of the color in the frame
                print discover_color(frame[ heigth*frame_row:heigth*(frame_row+1)  ,  width*frame_column:width*(frame_column+1) ], frame_row, frame_column)


        ch = 0xFF & cv2.waitKey(1)
        if ch == 27: #wait for ESC to stop
            break
    cv2.destroyAllWindows()
