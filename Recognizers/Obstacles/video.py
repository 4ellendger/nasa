import numpy as np
import cv2
from matplotlib import pyplot as plt

# Cameras indexes.
CAMERA_LEFT = 2
CAMERA_RIGHT = 1

# Windows names.
W_LEFT = "Left"
W_RIGHT = "Right"

"""
Start capture from 2 cameras.
In loop: captured, called "callback(frameL, frameR)",
waits "rate" milliseconds "q" key pressed to quit.
@param callback - callback(frameL, frameR). Prints returned value.

"""
def start(callback, rate):
    if (not rate):
        rate = 50
    capL = cv2.VideoCapture(CAMERA_LEFT)
    capR = cv2.VideoCapture(CAMERA_RIGHT)

    if (not callback):
        cv2.namedWindow(W_LEFT)
        cv2.moveWindow(W_LEFT, 100, 100)
        cv2.namedWindow(W_RIGHT)
        cv2.moveWindow(W_RIGHT, 800, 100)

    while(True):
        retL, frameL = capL.read()
        retR, frameR = capR.read()
        if (retL and retR):
            if (callback):
                print callback(frameL, frameR)
            else:
                cv2.imshow(W_LEFT, frameL)
                cv2.imshow(W_RIGHT, frameR)

            if (cv2.waitKey(rate) & 0xFF == ord('q')):
                break
            #break

    capL.release()
    capR.release()
    cv2.destroyAllWindows()

#start(None, 50)
