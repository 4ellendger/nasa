import numpy as np
import cv2
from matplotlib import pyplot as plt
import sys
sys.path.append(".")
from video import start


def cb(frameL, frameR):
    #frameL = cv2.pyrDown(frameL)
    #frameR = cv2.pyrDown(frameR)

    #frameL = cv2.medianBlur(frameL,5)
    #frameR = cv2.medianBlur(frameR,5)


    frameL = cv2.cvtColor(frameL,cv2.COLOR_RGB2GRAY)
    frameR = cv2.cvtColor(frameR,cv2.COLOR_RGB2GRAY)

    stereo = cv2.StereoBM(cv2.STEREO_BM_BASIC_PRESET, ndisparities=16, SADWindowSize=15)
    disparity = stereo.compute(frameL,frameR)
    plt.imshow(disparity,"gray")
    plt.draw()

plt.ion()
start(cb, 100)
