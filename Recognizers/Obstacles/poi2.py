import numpy as np
import cv2
from matplotlib import pyplot as plt
import sys
sys.path.append(".")
from video import start

# Blue Green Red. Target color should match is small gap. Other - match all.
MIN_COLOR = np.array([10,10,200])
MAX_COLOR = np.array([255,255,220])
COLUMNS = 16
ROWS = 12
MATCH_MIN = 150 # 0...255

def cb(frameL, frameR):
    frame = np.concatenate((frameL, frameR), axis=1)
    cv2.imwrite("frame.jpg", frame)

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frameL, cv2.COLOR_BGR2HSV)
    hsv = cv2.medianBlur(hsv,5)

    h, w, depth = frameL.shape

    mask = cv2.inRange(hsv, MIN_COLOR, MAX_COLOR)
    #cv2.imshow('mask', mask)
    #cv2.waitKey(0)

    deltaW = w / COLUMNS
    deltaH = h / ROWS
    targets = []
    for x in range(COLUMNS):
        for y in range(ROWS):
            cell = mask[deltaW*y:deltaW*(y+1), deltaH*x:deltaH*(x+1)]
            #print cv2.mean(cell)
            if (cv2.mean(cell)[0] > MATCH_MIN):
                targets.append("[%d, %d]" % (x, y))
                #print "x=%d y=%d" % (x, y)

    if (len(targets) > 0):
        return "9: %s" % ', '.join(targets)
        #return "9: %s" % targets.join(",")

    cv2.destroyAllWindows()
    return 0;


#cv2.namedWindow('mask', 0)
start(cb, 100)
