import numpy as np
import cv2
from matplotlib import pyplot as plt
import sys
sys.path.append(".")
from video import start

plt.ion()
plt.show()
fig = plt.figure()
fig.add_subplot(121)
fig.add_subplot(122)
def cb(frameL, frameR):

    hL, wL, dL = frameL.shape
    height = 320
    width = 240
    frameL = cv2.resize(frameL,(width, height), interpolation = cv2.INTER_LINEAR)
    frameR = cv2.resize(frameR,(width, height), interpolation = cv2.INTER_LINEAR)

    #frameL = cv2.cvtColor(frameL,cv2.COLOR_BGR2GRAY)
    #frameR = cv2.cvtColor(frameR,cv2.COLOR_BGR2GRAY)

    frameL = cv2.medianBlur(frameL,5)
    frameL = cv2.adaptiveThreshold(frameL,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    #retL, frameL = cv2.threshold(frameL,127,255,cv2.THRESH_BINARY)


    frameR = cv2.medianBlur(frameR,5)
    frameR = cv2.adaptiveThreshold(frameR,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    #retR, frameR = cv2.threshold(frameR,127,255,cv2.THRESH_BINARY)


    plt.subplot(122), plt.imshow(frameL,"gray")
    plt.subplot(121), plt.imshow(frameR,"gray")
    plt.draw()

start(cb, 50)
