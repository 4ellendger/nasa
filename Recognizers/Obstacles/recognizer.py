import numpy as np
import cv2
import time

# Cameras indexes.
CAMERA_LEFT = 2
CAMERA_RIGHT = 1

def start(callback, rate):
    if (not rate):
        rate = 50
    capL = cv2.VideoCapture(CAMERA_LEFT)
    capR = cv2.VideoCapture(CAMERA_RIGHT)

    if (not callback):
        cv2.namedWindow(W_LEFT)
        cv2.moveWindow(W_LEFT, 100, 100)
        cv2.namedWindow(W_RIGHT)
        cv2.moveWindow(W_RIGHT, 800, 100)

    while(True):
        retL, frameL = capL.read()
        retR, frameR = capR.read()
        if (retL and retR):
            if (callback):
                print callback(frameL, frameR)
            else:
                cv2.imshow(W_LEFT, frameL)
                cv2.imshow(W_RIGHT, frameR)

            if (cv2.waitKey(rate) & 0xFF == ord('q')):
                break
            #break

    capL.release()
    capR.release()
    cv2.destroyAllWindows()


# Images paths.
EXTENSION = ".jpg"
FRAME_PATH = "frame" + EXTENSION
TARGETS_PATH = "targets/"

# Obstacles range.
OBSTACLES_RANGE = 0.2
X_START = 0.5 - OBSTACLES_RANGE
X_END = 0.5 + OBSTACLES_RANGE

# Pit variables.
HORIZONT_Y = 0.5
HC_Y = 0.75
PIT_HC_2_HOR_MIN = 1.15

# Wall variables.
WALL_Y_START = 0.5
WALL_Y_END = 1.0
WW_RANGE = 0.3 # For wide walls
WW_X_START = 0.5 - WW_RANGE
WW_X_END = 0.5 + WW_RANGE
WW_W_2_H_MIN = 1.05
WS_Y_START = WALL_Y_START
WS_Y_END = WALL_Y_END
WL_Y = 0.75
WL_L_2_H_MIN = 1.7

# Blue Green Red. Target color should match only in small gap.
MIN_COLOR = np.array([10,10,200])
MAX_COLOR = np.array([255,255,220])
COLUMNS = 16
ROWS = 12
MATCH_MIN = 150 # 0...255


#creating HSV color map for recognizing the target color
hsv_map = np.zeros((180, 256, 3), np.uint8)
h, s = np.indices(hsv_map.shape[:2])
hsv_map[:,:,0] = h
hsv_map[:,:,1] = s
hsv_map[:,:,2] = 255
hsv_map = cv2.cvtColor(hsv_map, cv2.COLOR_HSV2BGR)
hist_scale = 64

# Function to discover if Pink color present in the given frame, returns the coordinates of frame

def discover_color(frame):
    frame = cv2.medianBlur(frame,5)
    small = cv2.pyrDown(frame)
    BGR_Borders = [[150 / 255.0 , 180 / 255.0], [60 / 255.0 , 80 / 255.0],[254 / 255.0 , 256 / 255.0]] #Pink color in BGR colors
    #for width_step in range(width/20):
    #small_step=small[0:heigth, width_step*20:width_step*20+width_step]
    hsv = cv2.cvtColor(small, cv2.COLOR_BGR2HSV)
    dark = hsv[...,2] < 32
    hsv[dark] = 0
    h = cv2.calcHist( [hsv], [0, 1], None, [180, 256], [0, 180, 0, 256] )
    h = np.clip(h*0.005*hist_scale, 0, 1)
    vis = hsv_map*h[:,:,np.newaxis]  / 255.0
    #cv2.imshow('hist', vis) #show histogram

    for element in vis[160:180]: #Check range of dots in histogram to find out if Pink is present
        for element2 in element:
            if (element2[2]>BGR_Borders[2][0] and element2[2]<BGR_Borders[2][1]):
                if (element2[1]>BGR_Borders[1][0] and element2[1]<BGR_Borders[1][1]):
                    if (element2[0]>BGR_Borders[0][0] and element2[0]<BGR_Borders[0][1]):
                        return True

# Recognized obstacles (returned 1, 2, 3) and targets (returned 9)
def recognizer(frameLeftSrcColor, frameRightSrcColor):

    # Write "frame"
    frame = np.concatenate((frameLeftSrcColor, frameRightSrcColor), axis=1)
    cv2.imwrite(FRAME_PATH, frame)

    # Resize x2 to be faster.
    frameLeftSmall = cv2.pyrDown(frameLeftSrcColor)
    frameRightSmall = cv2.pyrDown(frameRightSrcColor)

    # Make some blur to remove noize.
    frameLeftBlur = cv2.medianBlur(frameLeftSmall,7)
    frameRightBlur = cv2.medianBlur(frameRightSmall,7)

    # Convert to gray.
    frameL = cv2.cvtColor(frameLeftBlur,cv2.COLOR_RGB2GRAY)
    frameR = cv2.cvtColor(frameRightBlur,cv2.COLOR_RGB2GRAY)

    # Calculate disparity map
    stereo = cv2.StereoBM(cv2.STEREO_BM_BASIC_PRESET,32,9)
    disparity = stereo.compute(frameL, frameR).astype(np.float32) / 16.0

    # Get width and height (240*320)
    h, w = disparity.shape

    # Calculate horisont in range
    horisont = disparity[HORIZONT_Y*h:h, X_START*w:X_END*w]
    meanHor, devHor = cv2.meanStdDev(horisont, mask=None)

    # Calculate horisont critical
    critical = disparity[HORIZONT_Y*h:HC_Y*h, X_START*w:X_END*w]
    meanHC, devHC = cv2.meanStdDev(critical, mask=None)

    # Check pit.
    if (meanHC / meanHor >= PIT_HC_2_HOR_MIN):
        return "1: %f / %f >= %f" % (meanHC, meanHor, PIT_HC_2_HOR_MIN)

    # Calculate wide wall
    wideWall = disparity[WALL_Y_START*h:WALL_Y_END*h, WW_X_START*w:WW_X_END*w]
    meanWW, devWW = cv2.meanStdDev(wideWall, mask=None)

    if (meanWW / meanHor >= WW_W_2_H_MIN):
        return "2: %f / %f >= %f" % (meanWW, meanHor, WW_W_2_H_MIN)

    # Calculate strong wall
    wallStrong = disparity[WALL_Y_START*h:WALL_Y_END*h, X_START*w:X_END*w]
    meanWS, devWS = cv2.meanStdDev(wallStrong, mask=None)


    # Calculate wall line.
    wallLine = disparity[WL_Y*h:WL_Y*h + 10, X_START*w:X_END*w]
    meanWL, devWL = cv2.meanStdDev(wallLine, mask=None)

    if (meanWL / meanWW >= WL_L_2_H_MIN):
        return "3: %f / %f >= %f" % (meanWL, meanWW, WL_L_2_H_MIN)


    heigth, width, depth = frameL.shape
    zones_count=3 # zones_count^2 - number of zones to separate frame
    heigth = heigth/zones_count
    width = width/zones_count

    #discover_color(frameL, 0, 0)
    #cv2.waitKey(0)
    targets = []
    for frame_row in range(zones_count):
        for frame_column in range(zones_count):
            #Return position of the color in the frame
            #return discover_color(frameL[heigth*frame_row:heigth*(frame_row+1), width*frame_column:width*(frame_column+1)], frame_row, frame_column)
            coord = discover_color(frameL[heigth*frame_row:heigth*(frame_row+1), width*frame_column:width*(frame_column+1)])
            if coord:
                targets.append("[%d, %d]" % (frame_column, frame_row))
                coord = None

    if (len(targets) > 0):
        targetPhotoPath = "%s%d%s" % (TARGETS_PATH, time.time(), EXTENSION)
        cv2.imwrite(targetPhotoPath, frameL)
        #print "9: %s photo=%s" % (", ".join(targets), targetPhotoPath)
        return "9: %s photo=%s" % (", ".join(targets), targetPhotoPath)
    else:
        return 0





start(recognizer, 100)
