import sys
import cv2
import numpy as np
from matplotlib import pyplot as plt
sys.path.append(".")
from video import start

# Crashed if small distance to find common points.

"""
plt.ion()
plt.show()
fig = plt.figure()
fig.add_subplot(121)
fig.add_subplot(122)
"""

def cb(frameL, frameR):
    #cv2.imshow("dfs", frameL)

    sift = cv2.SIFT()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(frameL, None)
    kp2, des2 = sift.detectAndCompute(frameR, None)

    # FLANN parameters
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks=50)
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1, des2, k=2)

    good = []
    pts1 = []
    pts2 = []

    # ratio test as per Lowe's paper
    for i,(m,n) in enumerate(matches):
        if m.distance < 0.7 * n.distance:
            good.append(m)
            pts2.append(kp2[m.trainIdx].pt)
            pts1.append(kp1[m.queryIdx].pt)

    # Create fundamental Mat from best comparitions.
    pts1 = np.float32(pts1)
    pts2 = np.float32(pts2)
    F, mask = cv2.findFundamentalMat(pts1,pts2,cv2.FM_LMEDS)

    # We select only inlier points
    #pts1 = pts1[mask.ravel()==1]
    #pts2 = pts2[mask.ravel()==1]

    # Find epilines corresponding to points in right image (second image) and
    # drawing its lines on left image
    lines1 = cv2.computeCorrespondEpilines(pts2.reshape(-1,1,2), 2,F)
    lines1 = lines1.reshape(-1,3)
    img5,img6 = drawlines(frameL, frameR, lines1, pts1, pts2)

    # Find epilines corresponding to points in left image (first image) and
    # drawing its lines on right image
    lines2 = cv2.computeCorrespondEpilines(pts1.reshape(-1,1,2), 1,F)
    lines2 = lines2.reshape(-1,3)
    img3,img4 = drawlines(frameL, frameR, lines2, pts2, pts1)

    """
    plt.subplot(121), plt.imshow(img5)
    plt.subplot(122), plt.imshow(img3)
    plt.draw()
    #plt.show()
    """
    cv2.imshow("left", img5)
    cv2.imshow("right", img3)


def drawlines(img1, img2, lines, pts1, pts2):
    pts1 = np.int32(pts1)
    pts2 = np.int32(pts2)
    lines = np.int32(lines)
    r,c,depth = img1.shape
    img1 = cv2.cvtColor(img1,cv2.COLOR_RGB2GRAY)
    img2 = cv2.cvtColor(img2,cv2.COLOR_RGB2GRAY)
    for r,pt1,pt2 in zip(lines,pts1,pts2):
        color = tuple(np.random.randint(0,255,3).tolist())
        x0,y0 = map(int, [0, -r[2]/r[1] ])
        x1,y1 = map(int, [c, -(r[2]+r[0]*c)/r[1] ])
        cv2.line(img1, (x0,y0), (x1,y1), color,1)
        cv2.circle(img1,tuple(pt1), 10, color, -1)
        cv2.circle(img2,tuple(pt2),10,color,-1)
    return img1,img2

cv2.namedWindow("left")
cv2.namedWindow("right")

start(cb, 500)
