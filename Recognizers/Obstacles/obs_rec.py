import numpy as np
import cv2
from matplotlib import pyplot as plt
import sys
sys.path.append(".")
from video import start

# Obstacles range.
OBSTACLES_RANGE = 0.2
X_START = 0.5 - OBSTACLES_RANGE
X_END = 0.5 + OBSTACLES_RANGE

# Pit variables.
HORIZONT_Y = 0.5
HC_Y = 0.75
PIT_HC_2_HOR_MIN = 1.15

# Wall variables.
WALL_Y_START = 0.5
WALL_Y_END = 1.0
WW_RANGE = 0.3 # For wide walls
WW_X_START = 0.5 - WW_RANGE
WW_X_END = 0.5 + WW_RANGE
WW_W_2_H_MIN = 1.05
WS_Y_START = WALL_Y_START
WS_Y_END = WALL_Y_END
WL_Y = 0.75
WL_L_2_H_MIN = 1.7

"""
Returns 0 if all good, 1 if pit, 2 if wide wall, 3 if strong wall
"""
def cb(frameL, frameR):

    # Write "frame"
    frame = np.concatenate((frameL, frameR), axis=1)
    cv2.imwrite("frame.jpg", frame)

    # Resize x2 to be faster.
    frameL = cv2.pyrDown(frameL)
    frameR = cv2.pyrDown(frameR)

    # Make some blur to remove noize.
    frameL = cv2.medianBlur(frameL,7)
    frameR = cv2.medianBlur(frameR,7)

    # Convert to gray.
    frameL = cv2.cvtColor(frameL,cv2.COLOR_RGB2GRAY)
    frameR = cv2.cvtColor(frameR,cv2.COLOR_RGB2GRAY)

    # Calculate disparity map
    stereo = cv2.StereoBM(cv2.STEREO_BM_BASIC_PRESET,32,9)
    disparity = stereo.compute(frameL, frameR).astype(np.float32) / 16.0

    # Get width ahn height (240*320)
    h, w = disparity.shape

    # Calculate common mean and deviation
    meanAll, stdDevAll = cv2.meanStdDev(disparity, mask=None)

    # Calculate horisont in range
    horisont = disparity[HORIZONT_Y*h:h, X_START*w:X_END*w]
    meanHor, devHor = cv2.meanStdDev(horisont, mask=None)

    # Calculate horisont critical
    critical = disparity[HORIZONT_Y*h:HC_Y*h, X_START*w:X_END*w]
    meanHC, devHC = cv2.meanStdDev(critical, mask=None)

    # Check pit.
    if (meanHC / meanHor >= PIT_HC_2_HOR_MIN):
        return "1: %f / %f >= %f" % (meanHC, meanHor, PIT_HC_2_HOR_MIN)

    # Calculate wide wall
    wideWall = disparity[WALL_Y_START*h:WALL_Y_END*h, WW_X_START*w:WW_X_END*w]
    meanWW, devWW = cv2.meanStdDev(wideWall, mask=None)

    if (meanWW / meanHor >= WW_W_2_H_MIN):
        return "2: %f / %f >= %f" % (meanWW, meanHor, WW_W_2_H_MIN)

    # Calculate strong wall
    wallStrong = disparity[WALL_Y_START*h:WALL_Y_END*h, X_START*w:X_END*w]
    meanWS, devWS = cv2.meanStdDev(wallStrong, mask=None)


    # Calculate wall line.
    wallLine = disparity[WL_Y*h:WL_Y*h + 10, X_START*w:X_END*w]
    meanWL, devWL = cv2.meanStdDev(wallLine, mask=None)

    if (meanWL / meanWW >= WL_L_2_H_MIN):
        return "3: %f / %f >= %f" % (meanWL, meanWW, WL_L_2_H_MIN)

    return 0 # All good

start(cb, 100)
