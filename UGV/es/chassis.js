var ChassisPortFD = null;
var PanTiltPortFD = null;
var Pan = 0.0;
var Tilt = 0.0;
var ChassisSpeed = 50;
var fs = require('fs');
//-----------------------------------------------------------------------------------------------------------
function Initialize(){
//    ChassisPortFD = fs.openSync('/dev/ttyUSB0','r+');
    console.log("Порт управления шасси успешно открыт!");
//    PanTiltPortFD = fs.openSync('/dev/servoblaster','r+');
    console.log("Порт управления камерой успешно открыт!");
    PanTiltControlAbs(0,0);
};
//------------------------------------------------------------------------------------------------------------
function Finalize()
{
    fs.closeSync(PanTiltPortFD);
       console.log("Порт управления камерой успешно закрыт!");
    fs.closeSync(ChassisPortFD);
       console.log("Порт управления шасси успешно закрыт!");
}
//------------------------------------------------------------------------------------------------------------

function MotorControl(Number, Speed)
{
    Speed = Math.round(Speed / 2.5);//поправка на напряж��ние двигателей 
    if (Speed==0)
        Command = ':M'+Number+'SF0000\r\n';
    else if(Speed>0)
        Command = ':M'+Number+'PF'+Math.abs(Speed)+'00\r\n';
    else
        Command = ':M'+Number+'PR'+Math.abs(Speed)+'00\r\n';
    fs.writeSync(ChassisPortFD, Command, null, 'ascii');    
}

function PanTiltControlAbs(PanAngle, TiltAngle)//pan = -180..180 , Tilt = -45..45
{
    //настройки панорамирования и наклона
    var PanMiddle = 1470;
    var PanMultipler = 1.9;
    var TiltMiddle = 1400;
    var TiltMultipler = 11;
    var PanServo = 1;
    var TiltServo = 2;
    Pan = PanAngle;
    Tilt = TiltAngle;

    PanAngle = -PanAngle;
    //нормализация углов поворота
    if(PanAngle > 180) PanAngle = 180;
    if(PanAngle < -180) PanAngle = -180;
    if(TiltAngle > 45) TiltAngle = 45;
    if(TiltAngle < -45) TiltAngle = -45;    

    //Расчет значений ШИМ
    var PanPWM = PanMiddle + PanAngle * PanMultipler;
    var TiltPWM = TiltMiddle + TiltAngle * TiltMultipler;
    
    //отправляем команды servoblaster'у
//    fs.writeSync(PanTiltPortFD, PanServo+"="+Math.round(PanPWM/10)+'\n', null, 'ascii');    
//    fs.writeSync(PanTiltPortFD, TiltServo+"="+Math.round(TiltPWM/10)+'\n', null, 'ascii');    
/*
Pan 0-1500 90-1350 -90-1670 180-1140 -180-1850
Tilt 0-1450 1000..2000

*/
    
}

function PanTiltControlRel(DeltaPan, DeltaTilt)
{
    PanTiltControlAbs(Pan+DeltaPan,Tilt+DeltaTilt);
}

function GetPan()
{
    return Pan;
}

function GetTilt()
{
    return Tilt;
}

function SetChassisSpeed(Speed)
{
    ChassisSpeed = Speed;
}

function GetChassisSpeed()
{
    return ChassisSpeed;
}

function StopAfter(Seconds)
{
    setTimeout(Stop,Math.round(Seconds*1000));
}

function Forward(Seconds)
{
console.log('start');
    MotorControl(1,ChassisSpeed);
    MotorControl(2,ChassisSpeed);
    if (!(Seconds === undefined)) 
	StopAfter(Seconds);  
}

function Backward(Seconds)
{
console.log('start');
    MotorControl(1,-ChassisSpeed);
    MotorControl(2,-ChassisSpeed);

    if (!(Seconds === undefined)) 
	StopAfter(Seconds);  
}

function TurnLeft(Seconds)
{
console.log('start');
    MotorControl(1,-ChassisSpeed);
    MotorControl(2,ChassisSpeed);
    if (!(Seconds === undefined)) 
	StopAfter(Seconds);  
}

function TurnRight(Seconds)
{
console.log('start');
    MotorControl(1,ChassisSpeed);
    MotorControl(2,-ChassisSpeed);
    if (!(Seconds === undefined)) 
	StopAfter(Seconds);  
}

function Stop()
{
    MotorControl(1,00);
    MotorControl(2,00);
console.log('stop');
}

exports.НачалоРаботы = Initialize;
exports.ЗавершениеРаботы = Finalize;
exports.Вперед = Forward;
exports.Назад = Backward;
exports.ПоворотНаправо = TurnRight;
exports.ПоворотНалево = TurnLeft;
exports.УстановитьСкоростьДвижения = SetChassisSpeed;
exports.ПолучитьСкоростьДвижения = GetChassisSpeed;
exports.НаправлениеОбзораАбс = PanTiltControlAbs;
exports.НаправлениеОбзораОтн = PanTiltControlRel;
exports.НаправлениеОбзораГор = GetPan;
exports.НаправлениеОбзораВерт = GetTilt;
exports.Стоп = Stop;
