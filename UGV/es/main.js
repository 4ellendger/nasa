process.on('exit', 
    function() {
	Шасси.ЗавершениеРаботы();
	console.log('Завершение работы.');
    }
);


function Пауза(ms) {
    ms += new Date().getTime();
    while (new Date() < ms){}
} 

function ВыполнитьМиссию(Mission)
{
    var fs = require('fs');
    console.log("Запущена миссия ["+Mission+"]!");
    eval(fs.readFileSync('missions/'+Mission+'.mission').toString());
    console.log("Миссия ["+Mission+"] выполнена!");
}

var http = require("http");
var html = require('./html.js');
var url = require("url");
var mime = require("./mime.js");
var Шасси = require('./chassis.js');
var ОкружающаяСреда = require('./environment.js');

var RemoteControlPort = 2001;
var ResourcesRoot = '/home/sergey/UGV/es/resources/';

var RemoteHandlers = {};
RemoteHandlers["/"] = MainScreen;
RemoteHandlers["/action"] = ExecuteAction;
RemoteHandlers["/videostream"] = GetFrame;


//Ручное управление
function ManualControlSet()
{
    return html.Таблица(
			html.Строка(
	    		    html.Ячейка(
	    			html.Таблица(
	    			    html.Заголовок("Камера")+
				    html.Строка(
			    		html.Ячейка("")+
					html.Ячейка(html.Кнопка("Вверх","cam_up"))+
					html.Ячейка("")
				    )+
				    html.Строка(
					html.Ячейка(html.Кнопка("Влево","cam_left"))+
					html.Ячейка(html.Кнопка("Центр","cam_center"))+
					html.Ячейка(html.Кнопка("Вправо","cam_right"))
				    )+
				    html.Строка(
			    	    html.Ячейка("")+
			    	    html.Ячейка(html.Кнопка("Вниз","cam_down"))+
			    	    html.Ячейка("")
				    )
			    )
			)+
			html.Ячейка(
			    html.Таблица(
				html.Заголовок("Шасси")+
				html.Строка(
				    html.Ячейка("")+
				    html.Ячейка(html.Кнопка("Вперед","chassis_forward"))+
			    	    html.Ячейка("")
				)+		
				html.Строка(
				    html.Ячейка(html.Кнопка("Налево","chassis_left"))+
				    html.Ячейка(html.Кнопка("Стоп","chassis_stop"))+
				    html.Ячейка(html.Кнопка("Направо","chassis_right"))
				)+
				html.Строка(
				    html.Ячейка("")+
				    html.Ячейка(html.Кнопка("Назад","chassis_backward"))+
				    html.Ячейка("")
				)
			    )
			)
		    )
		)
} 



//Интерфейс управления
function MainScreen(Response,Parameters)
{
    Response.writeHead(200, {"Content-Type": "text/html"});
    Response.write(
	html.Страница("Управление автономным транспортным средством",
		'<table><tr><td>'+html.ВидеоОкно('Левая камера','frameleft')+
		'</td><td>'+html.ВидеоОкно('Карта глубины','depthmap')+
		'</td><td>'+html.ВидеоОкно('Правая камера','frameright')+'</td></tr></table>'+
		'<div class="tabs">'+
		    '<ul><li><a href="#tabs_manual">Ручное управление</a></li>'+
		    '<li><a href="#tabs_automatic">Автономный режим</a></li>'+
		    '<li><a href="#tabs_admin">Администрирование</a></li></ul>'+
		    '<div id="tabs_manual">'+ManualControlSet()+'</div>'+
		    '<div id="tabs_automatic"><select class="combobox"><option>AI</option></select></div>'+
		    '<div id="tabs_admin">'+html.Кнопка("Завершение работы","system_shutdown")+'</div>'+
		'</div>'
	    )
	);	
}

//обработка команд ручного управления
function ExecuteAction(Response,Parameters)
{
    Cmd = Parameters['action'];
    if(Cmd=='cam_up')
	Шасси.НаправлениеОбзораОтн(0,10)
    else if(Cmd=='cam_down')
	Шасси.НаправлениеОбзораОтн(0,-10)
    else if(Cmd=='cam_left')
	Шасси.НаправлениеОбзораОтн(-10,0)
    else if(Cmd=='cam_right')
	Шасси.НаправлениеОбзораОтн(10,0)    
    else if(Cmd=='cam_center')
	Шасси.НаправлениеОбзораАбс(0,0)
    else if(Cmd=='chassis_forward')
	Шасси.Вперед(0.5)
    else if(Cmd=='chassis_backward')
	Шасси.Назад(0.5)
    else if(Cmd=='chassis_left')
	Шасси.ПоворотНалево(0.25)
    else if(Cmd=='chassis_right')
	Шасси.ПоворотНаправо(0.25)
    else if(Cmd=='chassis_stop')
	Шасси.Стоп()
    else if(Cmd=='system_shutdown')
	process.exit()
    Response.writeHead(200, {"Content-Type": "text/html"});
    Response.write("");
}

function GetFrame(Response,Parameters)
{
	var FrameName = Parameters['framename'];
	var fs = require('fs');
	Response.writeHead(200, {"Content-Type": "image/jpeg"});
	Response.write(fs.readFileSync('/tmp/'+FrameName+'.jpg'));
}

//обработчик HTTP запросов
function onRequest(request, response) 
{
    //разбор компонентов HTTP запроса
    var ParsedURL = url.parse(request.url,true);
    var PathName = ParsedURL.pathname;
    var Parameters = ParsedURL.query;
    //чтение файлового ресурса
    if(PathName.indexOf('/resources/')==0)
    {
	var fs = require('fs');
	var ResourceFileName = ResourcesRoot+PathName.substring('/resources/'.length);
	response.writeHead(200, {"Content-Type": mime.lookup(ResourceFileName)});
	response.write(fs.readFileSync(ResourceFileName));
    }
    //если существует обработчик, то выполняем его
    else if(typeof(RemoteHandlers[PathName])=='function')//вызов функции-обработчика
    {
	RemoteHandlers[PathName](response,Parameters);
    }
    //Неизвестный URL
    else
    {
	response.writeHead(404, {"Content-Type": "text/html"});    
	response.write("404 Not Found");
    }
    response.end();
}

Шасси.НачалоРаботы();
console.log("Система запущена!");
http.createServer(onRequest).listen(RemoteControlPort);
console.log("Веб-сервер успешно запущен на порту "+RemoteControlPort+'!');


//ВыполнитьМиссию('СлежениеЗаДвижущимисяОбьектами');
//Миссия.Выполнить('Тест');

