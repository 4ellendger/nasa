function ButtonOnClick(URL)
{    
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET',URL, true);
    xmlHttp.send(null);
}

$(function() {

    $( "#system_shutdown" ).button(
    {
	text: true,
	icons: {
	    primary: "ui-icon-power"
        }
    });



    $( "#cam_up" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-circle-triangle-n"
        }
    });

    $( "#cam_down" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-circle-triangle-s"
        }
    });
    $( "#cam_left" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-circle-triangle-w"
        }
    });
    $( "#cam_right" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-circle-triangle-e"
        }
    });
    $( "#cam_center" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-bullet"
        }
    });
    $( "#chassis_forward" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-circle-triangle-n"
        }
    });
    $( "#chassis_backward" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-circle-triangle-s"
        }
    });
    $( "#chassis_left" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-circle-triangle-w"
        }
    });
    $( "#chassis_right" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-circle-triangle-e"
        }
    });
    $( "#chassis_stop" ).button(
    {
	text: false,
	icons: {
	    primary: "ui-icon-stop"
        }
    });
    $(".tabs").tabs();
});