var imageNr = 0;
var finished = new Array();
var previous_time = new Date();

function createImageLayer(FrameName) 
{
    var img = new Image();
    img.style.position = "absolute";
    img.style.zIndex = -1;
    img.width = 640;
    img.height = 480;
    img.src = "videostream?action=snapshot&framename="+FrameName+"&n=" + (++imageNr);

    img.onload = function () 
    {
        this.style.zIndex = imageNr;
        while (1 < finished.length) 
        {
    	    var del = finished.shift();
	    del.parentNode.removeChild(del);
	}
        finished.push(this);
	current_time = new Date();
        delta = current_time.getTime() - previous_time.getTime();
        fps   = (1000.0 / delta).toFixed(1);
        previous_time = current_time;
	createImageLayer(FrameName);
    }

    var webcam = document.getElementById("videoframe_"+FrameName);
    webcam.insertBefore(img, webcam.firstChild);      
}     

