


function Page(Title, Body)
{
    return '<!doctype html>'+
	'<html>'+
	    '<head>'+
		'<meta http-equiv="content-type" content="text/html; charset=utf-8" />'+
		'<title>'+Title+'</title>'+
		'<link rel="stylesheet" href="resources/js/jquery-ui-1.10.3/themes/mint-choc/jquery-ui.css" />'+
		'<script type="text/javascript" src="resources/js/jquery-ui-1.10.3/jquery-1.9.1.js"></script>'+		
		'<script type="text/javascript" src="resources/js/jquery-ui-1.10.3/ui/jquery-ui.js"></script>'+
		'<script type="text/javascript" src="resources/js/stream.js"></script>'+
		'<script type="text/javascript" src="resources/js/ui.js"></script>'+
		'<script type="text/javascript" src="resources/js/stream.js"></script>'+
	    '</head>'+
	    '<body onload="createImageLayer(\'frameleft\');createImageLayer(\'depthmap\');createImageLayer(\'frameright\')" style="margin:0px; padding:0px; background: none rgb(51, 51, 51);">'+




		Body+
	    '</body>'+
	'</html>';
}

function Button(Title,Action)
{
    return '<button id="'+Action+'" onClick="ButtonOnClick('+"'"+'/action?action='+Action+"'"+');">'+Title+'</button>';
}

function Table(Rows)
{
    return '<table>'+Rows+'</table>';
}

function Header(Text)
{
    return '<caption class="ui-widget-header">'+Text+'</caption>';
}

function Row(Cells)
{
    return '<tr>'+Cells+'</tr>';
}

function Cell(Content)
{
    return '<td>'+Content+'</td>';
}

function VideoWindow(Title, FrameName)
{
    return '<table><tr><td><h3 class="ui-widget-header ui-corner-all" >'+Title+'</h2></td></tr><tr><td><div style="width:640px;height:480px" id="videoframe_'+FrameName+'" ><noscript>Активируйте поддержку JavaScript в броузере!</noscript></div></td></tr></table>';
}

function EnvironmentWindow()
{
    return '<table><tr><td><h3 class="ui-widget-header ui-corner-all" >Окружающая среда</h2></td></tr><tr><td><div style="width:600px;height:480px"><noscript>Активируйте поддержку JavaScript в броузере!</noscript></div></td></tr></table>';
}


exports.Страница = Page;
exports.Кнопка = Button;
exports.Таблица = Table;
exports.Заголовок = Header;
exports.Строка = Row;
exports.Ячейка = Cell;
exports.ВидеоОкно = VideoWindow;
exports.ОкноОкружающейСреды = EnvironmentWindow;