import numpy as np
import cv2
from matplotlib import pyplot as plt
from video import start

plt.ion()
plt.show()
def cb(frameL, frameR):
    stereo = cv2.createStereoBM(numDisparities=16, blockSize=15)
    disparity = stereo.compute(frameL,frameR)
    plt.imshow(disparity,"gray")
    plt.show()

start(cb, 50)
