#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <math.h>
#include <stdio.h>
#include <cstring>
#include <fstream>
#include <unistd.h>

#define FRAME_WIDTH 640
#define FRAME_HEIGHT 480
#define FRAME_ASPECT_RATIO ((double)FRAME_WIDTH/(double)FRAME_HEIGHT)

#define FRAME_CENTER_X FRAME_WIDTH / 2
#define FRAME_CENTER_Y FRAME_HEIGHT / 2

#define VIEW_ANGLE_H 45.0
//#define VIEW_ANGLE_V 34.0
#define VIEW_ANGLE_V (double)VIEW_ANGLE_H/(double)FRAME_ASPECT_RATIO


#define EPS_RADIUS 50

#define MOTION_THRESHOLD 30
#define MIN_MOTION_AREA 300
#define MAX_MOTION_AREA 10000
#define JSON_FILE_NAME "/tmp/vision.json"

#define OSD_TEXT_SIZE 1.0
#define OSD_FONT FONT_HERSHEY_PLAIN

using namespace cv;


//преобразование числа в строку
template <typename ROFL>
std::string to_string(ROFL tmp)
{
    std::ostringstream out;
    out << tmp;
    return out.str();
}


const double Pi = 3.14159265;

//радианы в градусы
double RadToDeg(double Rad)
{
    return Rad * 180.0 / Pi;
}

//градусы в радианы
double DegToRad(double Deg)
{
    return Deg * Pi / 180.0;
}

//преобразуем координаты точки в кадре в углы относительно оптической оси обьектива
double CamToAngleH(double CamCoord)
{
    return RadToDeg(atan(CamCoord*tan(DegToRad(VIEW_ANGLE_H/2.0))/(FRAME_WIDTH/2)));
}

double CamToAngleV(double CamCoord)
{
    return RadToDeg(atan(CamCoord*tan(DegToRad(VIEW_ANGLE_V/2.0))/(FRAME_HEIGHT/2)));
}

//задержка выполнения программы
void Delay(double Seconds)
{
    usleep((1000000.0*Seconds));
}

int main(int, char**)
{
    clock_t CurrentTime,LastTime;

    string JSONText,MotionSegmentsText;

    int TargetX,TargetY,TargetR,TempX,TempY,TempR, X2,Y2;
    int ObjectCounter, ObjectArea, ObjectPerimeter;
    double FPS;
    char MsgString[255];

    Mat Frame, OSDFrame, PrevFrame,  MotionMask, leftFrame, rightFrame, depthMap;
    Mat MotionHistory(FRAME_HEIGHT,FRAME_WIDTH,CV_32FC1);
    Mat SeqMask(FRAME_HEIGHT,FRAME_WIDTH,CV_32FC1);

    vector<Rect> targets;
    vector<Rect>::iterator it, endIt;

    std::ofstream JSONFile;

    //-------------настройки сжатия отладочного изображения-----------
    vector<int> JPGCompressionParams;
    JPGCompressionParams.push_back(CV_IMWRITE_JPEG_QUALITY);
    JPGCompressionParams.push_back(80);//0-100 def 95
    //----------------------------------------------------------------


    VideoCapture leftCamera(1); // open the default camera
    VideoCapture rightCamera(0); // open the default camera

    if(!leftCamera.isOpened()) { // check if we succeeded
	printf("err left cam\n");
        return -1;}
    if(!rightCamera.isOpened()){  // check if we succeeded
	printf("err right cam\n");
        return -1;
	}
    //настройка камеры
    leftCamera.set(CV_CAP_PROP_FRAME_WIDTH,FRAME_WIDTH);
    leftCamera.set(CV_CAP_PROP_FRAME_HEIGHT,FRAME_HEIGHT);
    //cap.set(CV_CAP_PROP_BRIGHTNESS,255);
    //настройка камеры
    rightCamera.set(CV_CAP_PROP_FRAME_WIDTH,FRAME_WIDTH);
    rightCamera.set(CV_CAP_PROP_FRAME_HEIGHT,FRAME_HEIGHT);
    //----захват кадров---------
//    cap >> PrevFrame;
//    cvtColor(PrevFrame, PrevFrame, CV_BGR2GRAY);
//    GaussianBlur(PrevFrame, PrevFrame, Size(3,3), -1);
    
    while(1)
    {

	double timestamp = (double)clock()/CLOCKS_PER_SEC; // get current time in seconds
	LastTime = CurrentTime;
	CurrentTime = clock();
	FPS = (double)CLOCKS_PER_SEC / (double)(CurrentTime-LastTime);//расчет количества кадров в секунду
	printf("FPS=%2.1f\r",FPS);
	fflush(stdout);


	leftCamera.grab();
	rightCamera.grab();

	leftCamera.retrieve(leftFrame);
	rightCamera.retrieve(rightFrame);



	cvtColor(leftFrame, leftFrame, CV_BGR2GRAY);//пребразуем в оттенки серого
        cvtColor(rightFrame, rightFrame, CV_BGR2GRAY);//пребразуем в оттенки серого
        GaussianBlur(leftFrame, leftFrame, Size(7,7), -1);//применяем размытие по Гауссу
        GaussianBlur(rightFrame, rightFrame, Size(7,7), -1);//применяем размытие по Гауссу
	StereoBM bm(0,128,21);
	bm(leftFrame,rightFrame,depthMap);

/*        cap >> Frame; // получим кадр с камеры
	Frame.copyTo(OSDFrame);//копируем кадр для отладочного изображения
        cvtColor(Frame, Frame, CV_BGR2GRAY);//пребразуем в оттенки серого
        GaussianBlur(Frame, Frame, Size(7,7), -1);//применяем размытие по Гауссу


	absdiff(Frame, PrevFrame, MotionMask);//вычитаем предыдущий кадр из текущего
	Frame.copyTo(PrevFrame);//сохраняем кадр в качестве предыдущего
	threshold(MotionMask, MotionMask, MOTION_THRESHOLD, 255, THRESH_BINARY);
	updateMotionHistory( MotionMask, MotionHistory, timestamp, 1);
	segmentMotion(MotionHistory, SeqMask, targets, timestamp, 0.5 );

	MotionSegmentsText = "";
	ObjectCounter = 0;

	//обработка обнаруженных движущихся обьектов
	endIt = targets.end();
        for( it = targets.begin(); it != endIt; ++it )
	{
	    //вычислим площадь обьекта
	    ObjectArea = it->width*it->height;
	    if((ObjectArea>MIN_MOTION_AREA)&&(ObjectArea<MAX_MOTION_AREA))
	    {
		ObjectCounter++;
		//пересчет координат
		TempX = it->x + it->width/2 - FRAME_CENTER_X;
		TempY = -(it->y + it->height/2 - FRAME_CENTER_Y);
		ObjectPerimeter = 2 * (it->width+it->height);
		//создаем JSON строку
		MotionSegmentsText = MotionSegmentsText + 
		    "{\"X\":\""+to_string(TempX)+"\","+
		    "\"Y\":\""+to_string(TempY)+"\","+
		    "\"AH\":\""+to_string(CamToAngleH(TempX))+"\","+
		    "\"AV\":\""+to_string(CamToAngleV(TempY))+"\","+
		    "\"OA\":\""+to_string(ObjectArea)+"\","+
		    "\"OP\":\""+to_string(ObjectPerimeter)+"\","+
		    "\"W\":\""+to_string(it->width)+"\","+
		    "\"H\":\""+to_string(it->height)+"\"},";
		//отобразим движущийся обьект
                rectangle(OSDFrame, *it, Scalar( 255, 255, 0 ) );
	    }
	}
	//собираем и записываем JSON файл
	JSONText = "{ \"ДвижущиесяОбьекты\" : ["+ MotionSegmentsText +"]}";
	JSONFile.open("/tmp/vision.json");
	JSONFile << JSONText;
	JSONFile.close();
	
*/

	//== Вывод информации на отладочное изображение ==
	sprintf(MsgString,"FPS=%2.1f",FPS);
	putText(depthMap,MsgString,Point(10,20),OSD_FONT,OSD_TEXT_SIZE,Scalar(255,255,0));
	Point StartPoint, EndPoint;
	//горизонтальная линия
        StartPoint.x = FRAME_CENTER_X - EPS_RADIUS;
	StartPoint.y = FRAME_CENTER_Y;
        EndPoint.x = FRAME_CENTER_X + EPS_RADIUS;
        EndPoint.y = FRAME_CENTER_Y;
        line(depthMap,StartPoint,EndPoint,Scalar(0,255,0),1);
        //вертикальная линия
        StartPoint.y = FRAME_CENTER_Y - EPS_RADIUS;
        StartPoint.x = FRAME_CENTER_X;
        EndPoint.y = FRAME_CENTER_Y + EPS_RADIUS;
	EndPoint.x = FRAME_CENTER_X;
        line(depthMap,StartPoint,EndPoint,Scalar(0,255,0),1);



	//Вывод отладочного изображения в каталог вебсервера
        imwrite("/tmp/templeft.jpg", leftFrame,JPGCompressionParams);
        imwrite("/tmp/tempright.jpg", rightFrame,JPGCompressionParams);
        imwrite("/tmp/tempdepth.jpg", depthMap,JPGCompressionParams);

	rename("/tmp/templeft.jpg","/tmp/frameleft.jpg");
	rename("/tmp/tempright.jpg","/tmp/frameright.jpg");
	rename("/tmp/tempdepth.jpg","/tmp/depthmap.jpg");
	
    }

    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}

