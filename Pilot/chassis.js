var timer = require('timers');
var chassisPortFD = null;
var panTiltPortFD = null;
var IMUPortFD = null;
var speed;

var pan = 0.0;
var tilt = 0.0;
var chassisSpeed = 25;
var fs = require('fs');
//-----------------------------------------------------------------------------------------------------------
function initialize() {
	chassisPortFD = fs.openSync('/dev/ttyUSB1', 'r+');
	console.log("Порт управления шасси успешно открыт!");
	panTiltPortFD = fs.openSync('/dev/ttyUSB0', 'r+');
	console.log("Порт управления камерой успешно открыт!");
	IMUPortFD = fs.openSync('/dev/ttyUSB2', 'r+');
	console.log("Порт ИНС успешно открыт!");
	console.log(chassisSpeed + "asdasd");
	//    PanTiltControlAbs(0,0);
};

//------------------------------------------------------------------------------------------------------------
function finalize() {
	fs.closeSync(IMUPortFD);
	console.log("Порт ИНС успешно закрыт!");
	fs.closeSync(panTiltPortFD);
	console.log("Порт управления камерой успешно закрыт!");
	fs.closeSync(chassisPortFD);
	console.log("Порт управления шасси успешно закрыт!");
}
//------------------------------------------------------------------------------------------------------------

function motorControl(number, speed) {
	console.log(speed + " first launch  1 ");
	speed = Math.round(speed / 2.5);//поправка на напряж��ние двигателей 
	if (speed == 0)
		command = ':M' + number + 'SF0000\r\n';
	else if (speed > 0)
		command = ':M' + number + 'PF' + Math.abs(speed) + '00\r\n';
	else
		command = ':M' + number + 'PR' + Math.abs(speed) + '00\r\n';
	console.log(speed + " before call 2");
	fs.writeSync(chassisPortFD, command, null, 'ascii');
}

function panTiltControlAbs(panAngle, tiltAngle)//pan = -180..180 , Tilt = -45..45
{
	//нормализация углов поворота
	if (panAngle > 180)
		panAngle = 180;
	if (panAngle < -180)
		panAngle = -180;
	if (tiltAngle > 45)
		tiltAngle = 45;
	if (tiltAngle < -45)
		tiltAngle = -45;

	if (panAngle != pan) {
		cmd = 'SETPAN ' + panAngle + '\n';
		fs.writeSync(panTiltPortFD, cmd, null, 'ascii');
		pan = panAngle;
	}
	if (tiltAngle != tilt) {
		cmd = 'SETTILT ' + tiltAngle + '\n';
		fs.writeSync(panTiltPortFD, cmd, null, 'ascii');
		tilt = tiltAngle;
	}
}

function panTiltControlRel(deltaPan, deltaTilt) {
	panTiltControlAbs(pan + deltaPan, tilt + deltaTilt);
}

function getPan() {
	return pan;
}

function getTilt() {
	return tilt;
}

//0-100%
function setChassisSpeed(speed) {
	chassisSpeed = speed;
}

function getChassisSpeed() {
	return chassisSpeed;
}

function stopAfter(seconds) {
	setTimeout(stop, Math.round(seconds * 1000));
}

function forward(seconds) {
	console.log('start');
	motorControl(1, chassisSpeed);
	motorControl(2, chassisSpeed);
	if (!(seconds === undefined))
		stopAfter(seconds);
}

function alwaysForward() {
	console.log('start');
	motorControl(1, chassisSpeed);
	motorControl(2, chassisSpeed);
}

function backward(seconds) {
	console.log('start');
	motorControl(1, -chassisSpeed);
	motorControl(2, -chassisSpeed);

	if (!(seconds === undefined))
		stopAfter(seconds);
}

function turnLeft(seconds) {
	console.log('start');
	motorControl(1, -chassisSpeed);
	motorControl(2, chassisSpeed);
	if (!(seconds === undefined))
		stopAfter(seconds);
}

function turnRight(seconds) {
	console.log('start');
	motorControl(1, chassisSpeed);
	motorControl(2, -chassisSpeed);
	if (!(seconds === undefined))
		stopAfter(seconds);
}

function turnTo(angle) {
    var z = getKompasValue();
    var t = timer.setInterval(function() {
	if (z > angle) {
	    if (Math.abs(z - angle) > 5) {
		turnLeft(0.2)
	    }
	} else {
	    if (Math.abs(angle - z) > 5) {
		turnRight(0.2)
	    }
	}
    }, 300);
}

function stop() {
	motorControl(1, 00);
	motorControl(2, 00);
	console.log('stop');
}

function manipulatorDemo() {
	fs.writeSync(PanTiltPortFD, "MAN\n", null, 'ascii');
}

function getKompasValue() {
	//    fs.writeSync(IMUPortFD, "GETHEADING\n", null, 'ascii');
	var strHeading = "";
	buf = new Buffer(1);
	while (true) {
		fs.readSync(IMUPortFD, buf, 0, 1);
		Char = buf[0];
		if (Char == 10) {
			Heading = parseInt(strHeading);
			if (Heading > -180 && Heading < 180)
				return Heading;
		} else
			strHeading = strHeading + String.fromCharCode(Char);
	}
}

/**
 * Turn robot to specified angle.
 */
function turnToAngle(angle) {
	var x1 = getKompasValue();
	var x2 = Math.abs(x1 - angle);
	if (x2 > 180)
		if (x1 > 0)
			turnRight(360 - x2)
		else
			turnLeft(360 - x2);
	else if (x1 > 0)
		turnLeft(x2)
	else
		turnRight(x2);
}

/*
 exports.НачалоРаботы = Initialize;
 exports.ЗавершениеРаботы = Finalize;
 exports.Вперед = Forward;
 exports.Назад = Backward;
 exports.ПоворотНаправо = TurnRight;
 exports.ПоворотНалево = TurnLeft;
 exports.УстановитьСкоростьДвижения = SetChassisSpeed;
 exports.ПолучитьСкоростьДвижения = GetChassisSpeed;
 exports.НаправлениеОбзораАбс = PanTiltControlAbs;
 exports.НаправлениеОбзораОтн = PanTiltControlRel;
 exports.НаправлениеОбзораГор = GetPan;
 exports.НаправлениеОбзораВерт = GetTilt;
 exports.Стоп = Stop;
 exports.ДемонстрацияМанипулятора = ManipulatorDemo;
 exports.ПолучитьНаправлениеДвижения = GetHeading;
 */

exports.turnToAngle = turnToAngle;
exports.initialize = initialize;
exports.finalize = finalize;
exports.alwaysForward = alwaysForward;
exports.forward = forward;
exports.backward = backward;
exports.turnRight = turnRight;
exports.turnLeft = turnLeft;
exports.setChassisSpeed = setChassisSpeed;
exports.getChassisSpeed = getChassisSpeed;
exports.panTiltControlAbs = panTiltControlAbs;
exports.panTiltControlRel = panTiltControlRel;
exports.getPan = getPan;
exports.getTilt = getTilt;
exports.stop = stop;
exports.manipulatorDemo = manipulatorDemo;
exports.getKompasValue = getKompasValue;