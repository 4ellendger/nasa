var spawn = require('child_process').spawn;
var util = require('util');
var fs = require('fs')
var timer = require('timers')

exports.obstacle = false;
exports.targets = [];

var OBSTACLES_CRITICAL = 2;
var obstaclesCount = 0;
var DEBUG = false; // Set to "true" to see logs.

/**
 * Start recognizers to update "obstacle" and "targets".
 */
exports.start = function() {
    //var recognizers = spawn('python', ['recognizer.py'], {stdio: [process.stdout, null, null, null, 'pipe']});
    //var recognizers = spawn('bash', ['-c', '/home/mai/workspace/nasa_repo/Pilot/recognizer.py']);
    var recognizers  = spawn('python', ['recognizer.py']);
    //var recognizers  = spawn('ping', ['localhost']);

    recognizers.on('close', function (code, signal) {
        console.log('RECOGNIZERS STOPS! ' + signal);
    });

    recognizers.on('error', function (data) {
        console.log('RECOGNIZERS ERROR! ' + data);
    });

    recognizers.stderr.on('data', function (data) {
        console.log('RECOGNIZERS STDERR! ' + data);
    });

    recognizers.stdout.on('data', function (data) {
        data = data.toString();
        data = data.substring(0, data.length - 2);
        if (DEBUG) console.log("data=" + data);
        if (data.indexOf("0") !== -1) {// If obstacle or target.
            var code = parseInt(data[0]);
            if (code > 3) {
                exports.targets = JSON.stringify(data.substring(2));
                if (DEBUG) console.log("targets=" + exports.targets);
            } else {
            	obstaclesCount++
            	if (obstaclesCount >= OBSTACLES_CRITICAL) {
            		exports.obstacle = true;
            		if (DEBUG) console.log("obstacle=" + exports.obstacle);
            	}
            }
        } else {
        	obstaclesCount--;
        	if (obstaclesCount < 0) obstaclesCount = 0;
        	exports.obstacle = false;
        	exports.targets = [];
        	if (DEBUG) console.log("all cleared");
        }
    });
    console.log("Recognizers started");
}

