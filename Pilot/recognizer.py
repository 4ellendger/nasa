#! /usr/bin/env python

import numpy as np
import cv2
import time
import sys


# Cameras indexes.
CAMERA_LEFT = 2
CAMERA_RIGHT = 1

def start(callback, rate):
    if (not rate):
        rate = 50
    capL = cv2.VideoCapture(CAMERA_LEFT)
    capR = cv2.VideoCapture(CAMERA_RIGHT)

    if (not callback):
        cv2.namedWindow(W_LEFT)
        cv2.moveWindow(W_LEFT, 100, 100)
        cv2.namedWindow(W_RIGHT)
        cv2.moveWindow(W_RIGHT, 800, 100)

    while(True):
        retL, frameL = capL.read()
        retR, frameR = capR.read()
        if (retL and retR):
            if (callback):
                string = callback(frameL, frameR)
                print string
                sys.stdout.flush()
            else:
                cv2.imshow(W_LEFT, frameL)
                cv2.imshow(W_RIGHT, frameR)

            if (cv2.waitKey(rate) & 0xFF == ord('q')):
                break
            #break

    capL.release()
    capR.release()
    cv2.destroyAllWindows()


# Images paths.
EXTENSION = ".jpg"
FRAME_PATH = "frame" + EXTENSION
TARGETS_PATH = "targets/"

# Obstacles range.
OBSTACLES_RANGE = 0.2
X_START = 0.5 - OBSTACLES_RANGE
X_END = 0.5 + OBSTACLES_RANGE

# Pit variables.
HORIZONT_Y = 0.5
HC_Y = 0.75
PIT_HC_2_HOR_MIN = 1.15

# Wall variables.
WALL_Y_START = 0.5
WALL_Y_END = 1.0
WW_RANGE = 0.3 # For wide walls
WW_X_START = 0.5 - WW_RANGE
WW_X_END = 0.5 + WW_RANGE
WW_W_2_H_MIN = 1.05
WS_Y_START = WALL_Y_START
WS_Y_END = WALL_Y_END
WL_Y = 0.75
WL_L_2_H_MIN = 1.7

# Blue Green Red. Target color should match only in small gap.
MIN_COLOR = np.array([50,50,200])
MAX_COLOR = np.array([255,255,220])
COLUMNS = 16
ROWS = 12
MATCH_MIN = 100 # 0...255

# Recognized obstacles (returned 1, 2, 3) and targets (returned 9)
def recognizer(frameLeftSrcColor, frameRightSrcColor):

    # Write "frame"
    frame = np.concatenate((frameLeftSrcColor, frameRightSrcColor), axis=1)
    cv2.imwrite(FRAME_PATH, frame)

    # Resize x2 to be faster.
    frameLeftSmall = cv2.pyrDown(frameLeftSrcColor)
    frameRightSmall = cv2.pyrDown(frameRightSrcColor)

    # Make some blur to remove noize.
    frameLeftBlur = cv2.medianBlur(frameLeftSmall,7)
    frameRightBlur = cv2.medianBlur(frameRightSmall,7)

    # Convert to gray.
    frameL = cv2.cvtColor(frameLeftBlur,cv2.COLOR_RGB2GRAY)
    frameR = cv2.cvtColor(frameRightBlur,cv2.COLOR_RGB2GRAY)

    # Calculate disparity map
    stereo = cv2.StereoBM(cv2.STEREO_BM_BASIC_PRESET,32,9)
    disparity = stereo.compute(frameL, frameR).astype(np.float32) / 16.0

    # Get width and height (240*320)
    h, w = disparity.shape

    # Calculate horisont in range
    horisont = disparity[HORIZONT_Y*h:h, X_START*w:X_END*w]
    meanHor, devHor = cv2.meanStdDev(horisont, mask=None)

    # Calculate horisont critical
    critical = disparity[HORIZONT_Y*h:HC_Y*h, X_START*w:X_END*w]
    meanHC, devHC = cv2.meanStdDev(critical, mask=None)

    # Check pit.
    if (meanHC / meanHor >= PIT_HC_2_HOR_MIN):
        return "1: %f / %f >= %f" % (meanHC, meanHor, PIT_HC_2_HOR_MIN)

    # Calculate wide wall
    wideWall = disparity[WALL_Y_START*h:WALL_Y_END*h, WW_X_START*w:WW_X_END*w]
    meanWW, devWW = cv2.meanStdDev(wideWall, mask=None)

    if (meanWW / meanHor >= WW_W_2_H_MIN):
        return "2: %f / %f >= %f" % (meanWW, meanHor, WW_W_2_H_MIN)

    # Calculate strong wall
    wallStrong = disparity[WALL_Y_START*h:WALL_Y_END*h, X_START*w:X_END*w]
    meanWS, devWS = cv2.meanStdDev(wallStrong, mask=None)


    # Calculate wall line.
    wallLine = disparity[WL_Y*h:WL_Y*h + 10, X_START*w:X_END*w]
    meanWL, devWL = cv2.meanStdDev(wallLine, mask=None)

    if (meanWL / meanWW >= WL_L_2_H_MIN):
        return "3: %f / %f >= %f" % (meanWL, meanWW, WL_L_2_H_MIN)

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frameLeftBlur, cv2.COLOR_BGR2HSV)
    meanColor = cv2.mean(hsv);
    #MIN_COLOR = np.array([meanColor[0]*2.0,meanColor[1]*0.7,meanColor[2]*1.5])
    #MAX_COLOR = np.array([meanColor[0]*7.0,meanColor[1]*1.0,meanColor[2]*3.0])
    mask = cv2.inRange(hsv, MIN_COLOR, MAX_COLOR)
    #print meanColor
    #print hsv

    deltaW = w / COLUMNS
    deltaH = h / ROWS
    targets = []
    for x in range(COLUMNS):
        for y in range(ROWS):
            cell = mask[deltaW*y:deltaW*(y+1), deltaH*x:deltaH*(x+1)]
            #print cv2.mean(cell)
            if (cv2.mean(cell)[0] > MATCH_MIN):
                targets.append("[%d, %d]" % (x, y))
                #print "x=%d y=%d" % (x, y)

    if (len(targets) > 0):
        targetPhotoPath = "%s%d%s" % (TARGETS_PATH, time.time(), EXTENSION)
        cv2.imwrite(targetPhotoPath, frameLeftSrcColor)
        return "9: {coord:[%s], photo:'%s'}" % (", ".join(targets), targetPhotoPath)
    
    return 0 # Nothing found

try :
    stream = open("tmp.txt", "w+")
except IOError :
    print "Cannot open file"
    pass
start(recognizer, 100)
