var http = require('http');
var url = require("url")
var qs = require('querystring');
var fs = require('fs');
var pilot = require('./pilot');
var recognizers = require('./recognizers');
var emul = require('./emul');

var serverPort = 1337;
var travelStrategy = "travel";
var currentPhotoFrameName = "frame";
var pathToPhotos = "../Photos/";
var photoFilesExtention = ".jpg";
var startCommandName = "start";
var stopCommandName = "stop";
var commandPath = "/command";
var statusPath = "/status"
var framePath = "/frame";
var photoPath = "/photo/";
var postMethod = "POST";

function requestRecive(request, response) {
    var path = url.parse(request.url).pathname;
	if (path == commandPath) {
        startCommand(request, response);
	} else if (path == statusPath) {
		startGetStatus(response);
	} else if (path == framePath) {
		startGetFrame(response);
	} else if (path.indexOf(photoPath) != -1) {
		startGetPhoto(response,path.substring(photoPath.length));
	} else {
		console.log("Unknown url path ->" + path);
	}
}

/**
 * Start execute specified command.
 */
function startCommand(request, response) {
	if (request.method == postMethod) {
		var body = '';
		request.on('data', function (data) {
			body += data;
		});
		request.on('end', function () {
			var success = false;
			var jsonBody = JSON.parse(body);
			if (jsonBody.command == startCommandName) success = commandStartReceive(jsonBody);
				else if (jsonBody.command.toString() == stopCommandName) success = commandStopReceive();
			if (success) {
				response.writeHead(200, {'Content-Type': 'text/plain'});
				response.end();
			} else {
				response.writeHead(418, {'Content-Type': 'text/plain'});
				response.end();
			}
		});
    }

}

function commandStartReceive(options) {
    console.log("Start strategy ---->" + options.strategy);
	if (options.strategy == travelStrategy) {
		pilot.startTravel(options.angle);
	}
	//TODO:return something.
	return true;
}

//TODO:stop working
function commandStopReceive() {
    console.log("Stop!!!!");
	return true;
}

/**
 * Return current robot status.
 */
function startGetStatus(response) {
	response.writeHead(201, {'Content-Type': 'text/plain'});
	response.end();
}

/**
 * Return current robot frame.
 */
function startGetFrame(response) {
	fs.readFile(pathToPhotos + currentPhotoFrameName + photoFilesExtention, function(err, data) {
		if (err) throw err;
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(data);
	});
}

/**
 * Return specified by time id photo.
 */
function startGetPhoto(response, photoid) {
	fs.readFile(pathToPhotos + photoid + photoFilesExtention, function(err, data) {
		if (err) throw err;
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(data);
	});
}

var server = http.createServer(requestRecive);

server.listen(serverPort);

console.log('Server running at http://127.0.0.1:' + serverPort);

//recognizers.start();
emul.start();