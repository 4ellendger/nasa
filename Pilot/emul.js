//////////////////////// chassis emulator /////////////////////////////////////

var _kompass = "";
exports.getKompasValue = function() { return _kompass; }

var _angle = 0;
exports.turnToAngle = function(angle) { _angle = angle; console.log("EMUL: _speed=" + _speed); }

var mspeeds = [];
exports.motorControl = function(motor, speed) {
	mspeeds[motor] = speed;
	console.log("EMUL: mspeeds[" + motor + "]=" + speed);
}

exports.stop = function() { 
	exports.motorControl(1, 0);
	exports.motorControl(2, 0);
	console.log("EMUL: stop");
}

exports.stopAfter = function() {
	setTimeout(exports.stop, Math.round(seconds * 1000));
}

var _speed = 0;
exports.getChassisSpeed = function() { return _speed; };

exports.turnRight = function(seconds) {
	exports.motorControl(1, _speed);
	exports.motorControl(2, -_speed);
	if (!(seconds === undefined))
		exports.stopAfter(Seconds);
}

exports.turnLeft = function(seconds) {
	exports.motorControl(1, -_speed);
	exports.motorControl(2, _speed);
	if (!(seconds === undefined))
		exports.stopAfter(seconds);
}

var _chassisSpeed = 0;
exports.setChassisSpeed = function(speed) {
	_chassisSpeed = speed
	console.log("EMUL: _chassisSpeed=" + _chassisSpeed);
}
exports.getChassisSpeed = function () { return _chassisSpeed; }

exports.alwaysForward = function() { 
	exports.motorControl(1, _chassisSpeed);
	exports.motorControl(2, _chassisSpeed); 
	console.log("EMUL: isForward");
}

exports.backward = function() { 
	exports.motorControl(1, -_chassisSpeed);
	exports.motorControl(2, -_chassisSpeed); 
	console.log("EMUL: backward");
}

var _pan = 0;
var _tilt = 0;
exports.panTiltControlRel = function(panAngle, tiltAngle) { exports.panTiltControlAbs(_pan + panAngle, _tilt + tiltAngle); };
exports.panTiltControlAbs = function(pan, tilt) {//pan = -180..180 , Tilt = -45..45
	if (pan > 180) pan = 180;
	if (pan < -180) pan = -180;
	if (pan > 45) pan = 45;
	if (pan < -45) pan = -45;
	_pan = pan;
	_tilt = tilt;
	console.log("EMUL: _pan=" + _pan + " _tilt=" + _tilt); 
};

exports.getPan = function() { return _pan; };
exports.getTilt = function() { return _tilt; };
exports.manipulatorDemo = function() { console.log("EMUL: manipulatorDemo"); }

//////////////////////// recognizers emulator /////////////////////////////////////

exports.start = function() { console.log("EMUL: recognizers.start"); }
exports.obstacle = false
exports.targets = []

////////////////////////// Emulator //////////////////////////

/**
 * Starts to listen stdin.
 * @param callback Callback on recevied data (puts as callback param).
 */
function read(callback) {
	var stdin = process.stdin, stdout = process.stdout;

	stdin.resume();
	stdout.write("> ");

	stdin.once('data', function(data) {
		data = data.toString().trim();
		callback(data);
		read(callback);
	});
}
exports.read = read;

/**
 * Emulated:
 *     recognizers: if write "recognizers = emul".
 *     chassis"   : if write "chassis = emul".
 * Commands:
 *     'o1': obstacle found
 *     'o': obstacle not found
 *     'tXXXX': set target found (TODO)
 *     't': set not targets found
 *     'a-45': set angle to -45 degrees.
 *     'kXXXX': set kompas value (TODO)
 */
exports.start = function() {
	read(function(data) {
		switch (data[0]) {
		case "o":
			exports.obstacle = data.length > 1;
			console.log("EMUL: obstacle=" + exports.obstacle);
			break;
		case "t":
			if (data.length > 1) {
				exports.targets = data.substring(1);// TODO
			} else {
				exports.targets = [];
			}
			console.log("EMUL: targets=" + exports.targets);
			break;
		case "a":
			_angle = parseInt(data.substring(1));
			console.log("EMUL: _angle=" + _angle);
			break;
		case "k":
			_kompass = data.substring(1);// TODO
			console.log("EMUL: _kompass=" + _kompass);
			break;
		default:
			console.log("not known command");
			break;
		}
	});
};
