var timer = require('timers');
var chassis = require('./emul');
var recognizer = require('./emul');
var isRunning = false;
// chassis.initialize();
var leftClearAngle = -500;
var rightClearAngle = 500;
var checkSides = false;
var isTravel;
var t;

function checkLeft() {
    if (!recognizer.obstacle) {
	if (chassis.getPan() > leftClearAngle)
	    leftClearAngle = chassis.getPan();
    } else {
	chassis.panTiltControlRel(30, 0);
    }
};

function checkRight() {
    if (!recognizer.obstacle) {
	if (chassis.getPan() < rightClearAngle)
	    rightClearAngle = chassis.getPan();
    } else {
	chassis.panTiltControlRel(-30, 0);
    }
};

function travel() {
    isTravel = true;
    if (recognizer.obstacle) {
	timer.setTimeout(function() {
	    checkLeft();
	},0);
	timer.setTimeout(function() {
	    checkLeft();
	},2000);
	timer.setTimeout(function() {
	    checkLeft();
	},4000);
	timer.setTimeout(function() {
	    if (!recognizer.obstacle)
		leftClearAngle = chassis.getPan();
	},6000);
	timer.setTimeout(function() {
	    chassis.panTiltControlAbs(0,0);
	},6500);
	timer.setTimeout(function() {
	    checkRight();
	},15000);
	timer.setTimeout(function() {
	    checkRight();
	},17000);
	timer.setTimeout(function() {
	    checkRight();
	},19000);
	timer.setTimeout(function() {
	    if (!recognizer.obstacle)
		rightClearAngle = chassis.getPan();
	},21000);
	timer.setTimeout(function() {
	    console.log(rightClearAngle + "ss" + leftClearAngle);
	},23000);
	timer.setTimeout(function() {
	    if (Math.abs(leftClearAngle) > rightClearAngle) {
	    
	}
	},23500);
	    timer.clearInterval(t);
    }
    isTravel = false;
}

exports.startTravel = function(angle) {
    console.log("Start travel with angle = " + angle);

    // Turn to need side.
    chassis.setChassisSpeed(25);
    chassis.turnTo(angle);
    chassis.alwaysForward();
    t = timer.setInterval(function() {
	if (!isTravel)
	    travel();
    }, 1000);
}